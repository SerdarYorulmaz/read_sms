package com.example.sms_app.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import com.example.sms_app.R
import com.example.sms_app.model.SmsData
import kotlinx.android.synthetic.main.row_layout.view.*

class ListAdapter(val context:Context,val list:ArrayList<SmsData>):BaseAdapter() {
    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
        val view=LayoutInflater.from(context).inflate(R.layout.row_layout,parent,false)

        view.sms_sender.setText(list[position].senderName)
        view.sms_message.setText(list[position].message)

        view.sms_date.setText(list[position].date)

        return view
    }

    override fun getItem(position: Int): Any {
       return list[position]
    }

    override fun getItemId(position: Int): Long {
       return position.toLong()
    }

    override fun getCount(): Int {
       return list.size
    }
}