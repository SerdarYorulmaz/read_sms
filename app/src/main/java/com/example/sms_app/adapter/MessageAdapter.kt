package com.example.sms_app.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.sms_app.R
import com.example.sms_app.model.SmsData
import kotlinx.android.synthetic.main.row_layout.view.*

class MessageAdapter(val context: Context, val list:ArrayList<SmsData>): RecyclerView.Adapter<MessageAdapter.ViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {


        val view = LayoutInflater.from(context).inflate(R.layout.row_layout, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int = list.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        var tempData = list[position]
        holder.setData(tempData, position)


    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {


        // itemView.setOnClickListener( {itemClick(layoutPosition)} )

        fun setData(item: SmsData, position: Int) = with(itemView) {
            itemView.sms_message.setText(list[position].message)
            itemView.sms_sender.setText(list[position].senderName)
            itemView.sms_date.setText(list[position].date)

        }
    }

 }

