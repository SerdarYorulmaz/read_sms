package com.example.sms_app

import android.content.pm.PackageManager
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.core.app.ActivityCompat
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.sms_app.adapter.MessageAdapter
import com.example.sms_app.model.SmsData
import kotlinx.android.synthetic.main.activity_main.*
import java.util.*

class MainActivity : AppCompatActivity() {


private val requestReadSms:Int=2

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        if(ActivityCompat.checkSelfPermission(this,android.Manifest.permission.READ_SMS)!=PackageManager.PERMISSION_GRANTED){
            ActivityCompat.requestPermissions(this, arrayOf(android.Manifest.permission.READ_SMS),requestReadSms)
        }else{
            setSmsMessages("",null)
        }

        all_sms.setOnClickListener {
            setSmsMessages("",null)
        }

        inbox_sms.setOnClickListener {
            setSmsMessages("inbox",null)
        }

        outbox_sms.setOnClickListener {
            setSmsMessages("outbox",null)
        }

        send_sms.setOnClickListener {
            setSmsMessages("send",null)
        }

        draft_sms.setOnClickListener {
            setSmsMessages("draft",null)
        }

        one_number_sms.setOnClickListener {
            setSmsMessages("","address like '${getString(R.string.phone_number)}'")
        }

    }

    private fun setSmsMessages(uriString: String,selection: String?){
        val smsList=ArrayList<SmsData>()

        val cursor=contentResolver.query(
            Uri.parse("content://sms/${uriString}"),
            null,
            selection,
            null,
            null
        )

        if(cursor!!.moveToFirst()){
            val nameId=cursor.getColumnIndex("address")
            val messageId=cursor.getColumnIndex("body")
            val dateId=cursor.getColumnIndex("date")

            do {

                val dateString=cursor.getString(dateId)

                smsList.add(
                    SmsData(
                        cursor.getString(nameId),
                        Date(dateString.toLong()).toString(),
                        cursor.getString(messageId)
                    )
                )

            }while (cursor.moveToNext())
        }

        cursor.close()

        val adapter= MessageAdapter(this,smsList)

        sms_list_view.layoutManager= LinearLayoutManager(this)
        sms_list_view.adapter=adapter

    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {

        if(requestCode==requestReadSms) setSmsMessages("",null)
    }
}
